const mqtt = require('mqtt')

const express = require('express')
const path = require('path');
const app = express()
app.use(express.static('public'))
const port = 3000

const http = require('http')
const server = http.createServer(app)
const { Server } = require("socket.io")
const io = new Server(server);

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'));
})

server.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})

var client  = mqtt.connect('mqtt://192.168.1.30')

let data = 0

client.on('connect', function () {
  client.subscribe('hello/test')
})

client.on('message', function (topic, message) {
  data = message.toString()
  io.emit('json',data)
})

